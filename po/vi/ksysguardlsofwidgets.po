# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-06 00:20+0000\n"
"PO-Revision-Date: 2022-10-31 16:37+0100\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.08.1\n"

#: lsof.cpp:22
#, kde-format
msgctxt "Short for File Descriptor"
msgid "FD"
msgstr "FD"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: lsof.cpp:22 LsofSearchWidget.ui:28
#, kde-format
msgid "Type"
msgstr "Kiểu"

#: lsof.cpp:22
#, kde-format
msgid "Object"
msgstr "Đối tượng"

#: LsofSearchWidget.cpp:25
#, kde-format
msgid "Renice Process"
msgstr "Thay đổi độ ưu tiên của tiến trình"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: LsofSearchWidget.ui:23
#, kde-format
msgid "Stream"
msgstr "Luồng"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: LsofSearchWidget.ui:33
#, kde-format
msgid "Filename"
msgstr "Tên tệp"

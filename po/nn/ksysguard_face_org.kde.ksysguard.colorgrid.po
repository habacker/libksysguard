# Translation of ksysguard_face_org.kde.ksysguard.colorgrid to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-13 00:48+0000\n"
"PO-Revision-Date: 2023-03-10 22:07+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: contents/ui/Config.qml:25
#, kde-format
msgid "Use sensor color:"
msgstr "Bruk sensorfarge:"

#: contents/ui/Config.qml:30
#, kde-format
msgid "Number of Columns:"
msgstr "Talet på kolonnar:"

#: contents/ui/Config.qml:37
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "Automatisk"

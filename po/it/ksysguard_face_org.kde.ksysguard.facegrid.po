# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
# Paolo Zamponi <feus73@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-01-29 00:45+0000\n"
"PO-Revision-Date: 2021-08-31 18:04+0200\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"

#: contents/ui/Config.qml:35
#, kde-format
msgid "Number of Columns:"
msgstr "Numero di colonne:"

#: contents/ui/Config.qml:42
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "Automatico"

#: contents/ui/Config.qml:54
#, kde-format
msgid "Display Style:"
msgstr "Stile di visualizzazione:"

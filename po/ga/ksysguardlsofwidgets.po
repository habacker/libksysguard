# Irish translation of ksysguardlsofwidgets
# Copyright (C) 2009 This_file_is_part_of_KDE
# This file is distributed under the same license as the ksysguardlsofwidgets package.
# Kevin Scannell <kscanne@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: ksysguardlsofwidgets.po\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-06 00:20+0000\n"
"PO-Revision-Date: 2008-08-04 12:52-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#: lsof.cpp:22
#, kde-format
msgctxt "Short for File Descriptor"
msgid "FD"
msgstr "TC"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: lsof.cpp:22 LsofSearchWidget.ui:28
#, kde-format
msgid "Type"
msgstr "Cineál"

#: lsof.cpp:22
#, kde-format
msgid "Object"
msgstr "Réad"

#: LsofSearchWidget.cpp:25
#, kde-format
msgid "Renice Process"
msgstr "Athraigh Tosaíocht an Phróisis"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: LsofSearchWidget.ui:23
#, kde-format
msgid "Stream"
msgstr "Sruth"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: LsofSearchWidget.ui:33
#, kde-format
msgid "Filename"
msgstr "Ainm comhaid"
